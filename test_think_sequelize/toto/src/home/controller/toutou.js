'use strict';

import Base from './base.js';

export default class extends Base {
  async createAction(){
    console.log("create toutou appelé");
    var name = this.param("name");
    var owner_id = this.param("owner_id");
    global.db.Toutou.create({name:name, ownerId:owner_id});
    this.success();
  }

  async showsAction(){
    var id = this.get("id");
    var res = await global.db.Toutou.findById(id);
    this.success(res);
  }

  async editAction(){
    var name = this.param("name");
    var owner_id = this.param("owner_id");
    var id = this.get("id");
    var res = await global.db.Toutou.findById(id);
    if (name)
      res.name = name;
    if (owner_id)
      res.ownerId = owner_id;
    this.success(res.save());
  }

  async deleteAction(){
    var id = this.get("id");
    var res = await global.db.Toutou.destroy({where: {id:id}});
    this.success(res);
  }
}
