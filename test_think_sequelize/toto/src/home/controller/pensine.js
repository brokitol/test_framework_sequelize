'use strict';

import Base from './base.js';

export default class extends Base {
  async createAction(){
    console.log("create appellé");
    global.db.User.auto_create();
    this.success();
  }

  async showsAction(){
    var id = this.get("id");
    var res = await global.db.User.find({
      where: {uuid:id},
      include: [{
        model: global.db.Toutou,
        as: "pet",
        include: [global.db.Joujou]
      }]
    });
    this.success(res);
  }

  async editAction(){
    var id = this.get("id");
    var res = await global.db.User.findById(id);
    this.success(res);
  }

  async deleteAction(){
    var id = this.get("id");
    var res = await global.db.User.destroy({where: {uuid:id}});
    this.success(res);
  }
}
