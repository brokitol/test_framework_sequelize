'use strict';

import Base from './base.js';

export default class extends Base {

  async __before() {
    console.log("before appelé");
  }

  async __after() {
    console.log("after appelé");
  }

  async createAction(){
    var name = this.param("name");
    var toutou_id = this.param("toutou_id");
    console.log("create joujou appelé");
    global.db.Joujou.create({name:name, ToutouId:toutou_id});
    this.success();
  }

  async showsAction(){
    var id = this.get("id");
    var res = await global.db.Joujou.findById(id);
    this.success(res);
  }

  async editAction(){
    var name = this.param("name");
    var toutou_id = this.param("toutou_id");
    var id = this.get("id");
    var res = await global.db.Joujou.findById(id);
    if (name)
      res.name = name;
    if (toutou_id)
      res.ToutouId = toutou_id;
    this.success(res.save());
  }

  async deleteAction(){
    var id = this.get("id");
    var res = await global.db.Joujou.destroy({where: {id:id}});
    this.success(res);
  }
}
