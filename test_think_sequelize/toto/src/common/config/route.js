export default [
  ["pensine/create", "pensine/create"],
  ["pensine/:id", {
    get: "pensine/shows",
    put: "pensine/edit",
    delete: "pensine/delete"
  }],

  ["toutou/create", "toutou/create"],
  ["toutou/:id", {
    get: "toutou/shows",
    put: "toutou/edit",
    delete: "toutou/delete"
  }],

  ["joujou/create", "joujou/create"],
  ["joujou/:id", {
    get: "joujou/shows",
    put: "joujou/edit",
    delete: "joujou/delete"
  }]
];
