'use strict';

module.exports = function(app) {
	var sql = app.dataSources.sql;

	if (process.env.MIGRATE) {
		console.log('migrating...')
		sql.automigrate('User')
		sql.automigrate('AccessToken')
		sql.automigrate('RoleMapping')
		sql.automigrate('Role')
		sql.automigrate('firmUser')
		sql.automigrate('firmEstablishment')
		sql.automigrate('campaign')
		sql.automigrate('application')
		sql.automigrate('candidate')
		sql.automigrate('skill')
	}
}