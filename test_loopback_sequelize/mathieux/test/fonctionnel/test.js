var test = require('unit.js');

describe('ExperienceController', function() {
  describe('addExperience', function() {
    it("toto", function(done) {
      server.models.skill.find().then(function(res) {
        console.log(res);
        done();
      })
    })
    it.skip('ajout simple', function(done) {
      var data = {
        title: "le pouvoir du passé simple",
        employees: 5,
        contract_type: "CDI",
        start_date: "01/01/2016",
        end_date: "01/02/2016",
        description: "fait par default",
        base_job_id: 4,
        base_company_location_id: 4
      }
      test.httpAgent(process.env.ADDRESS_SERVER)
      		.post("/api/c/1/experience/add")
          .send(data)
      		.expect('Content-Type', /json/)
      		.expect(200)
      		.end(function(err, res){
            if (err) {
      				test.fail(err.message);
      			}
            //console.log(res);
            //console.log(res.body);
      			test.number(res.body.data).is(3);
      			done();
      		});
    });

    it.skip('employee', function(done) {
      var data = {
        title: "le pouvoir du passé simple",
        employees: -1,
        contract_type: "CDI",
        start_date: "01/01/2016",
        end_date: "01/02/2016",
        description: "fait par default",
        base_job_id: 4,
        base_company_location_id: 4
      }
      test.httpAgent(process.env.ADDRESS_SERVER)
      		.post("/api/c/1/experience/add")
          .send(data)
      		.expect('Content-Type', /json/)
      		.expect(200)
      		.end(function(err, res){
            if (err) {
      				test.fail(err.message);
      			}
            //console.log(res);
            //console.log(res.body);
      			test.number(res.body.data).is(3);
      			done();
      		});
    });
  });
});
