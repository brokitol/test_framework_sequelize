//const sequelize_fixtures = require('sequelize-fixtures');

/*var port = 7331;
process.env.NODE_ENV = 'test'
process.env.AUTO_START = false
process.env.ADDRESS_SERVER = "localhost:" + port*/

var server
//var request = require('supertest')(server)
//var expect = require('expect.js')

var server;

before(function() {
	//server = require('../lib/server.js').listen(port);
	server = require('../server/server')
	server.loadFixtures();
});

beforeEach(function(done) {
	/*server.app.db.sequelize.truncate({ cascade: true, restartIdentity: true })
		.then(function(){
    	sequelize_fixtures.loadFile('test/fixtures/*.json', server.app.db).then(function(){
				done();
    	});
		});*/
		done();
});

after(function() {
	//server.close();
});
/*describe('/Student', function () {

    var Student

    before(function() {
        Student = server.models.Student
    })

    it.skip('Post a new student', function (done) {
        request.post('/api/Students').send({points: 5000}).expect(200, done)
     })
})
*/

describe('ExperienceController', function() {
  describe('addExperience', function() {
    it("toto", function(done) {
      server.models.skill.find().then(function(res) {
        console.log(res);
        done();
      })
    })
    it.skip('ajout simple', function(done) {
      var data = {
        title: "le pouvoir du passé simple",
        employees: 5,
        contract_type: "CDI",
        start_date: "01/01/2016",
        end_date: "01/02/2016",
        description: "fait par default",
        base_job_id: 4,
        base_company_location_id: 4
      }
      test.httpAgent(process.env.ADDRESS_SERVER)
      		.post("/api/c/1/experience/add")
          .send(data)
      		.expect('Content-Type', /json/)
      		.expect(200)
      		.end(function(err, res){
            if (err) {
      				test.fail(err.message);
      			}
            //console.log(res);
            //console.log(res.body);
      			test.number(res.body.data).is(3);
      			done();
      		});
    });

    it.skip('employee', function(done) {
      var data = {
        title: "le pouvoir du passé simple",
        employees: -1,
        contract_type: "CDI",
        start_date: "01/01/2016",
        end_date: "01/02/2016",
        description: "fait par default",
        base_job_id: 4,
        base_company_location_id: 4
      }
      test.httpAgent(process.env.ADDRESS_SERVER)
      		.post("/api/c/1/experience/add")
          .send(data)
      		.expect('Content-Type', /json/)
      		.expect(200)
      		.end(function(err, res){
            if (err) {
      				test.fail(err.message);
      			}
            //console.log(res);
            //console.log(res.body);
      			test.number(res.body.data).is(3);
      			done();
      		});
    });
  });
});
