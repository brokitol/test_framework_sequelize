'use strict';

module.exports = function(Candidate) {

	Candidate.beforeRemote('*.__create__applications', (ctx, inst, next) => {
		console.log('global setted');
		global.inst = inst;
		global.ctx = ctx;
		global.candidate = Candidate;
		

		ctx
			.instance
			.applications
			.findOne({where: {campaignId: ctx.args.data.campaignId}})
			.then((application) => {
				if (!!application)
					next(new Error('already applied !'));
				else
					next();
			})
	})

};
