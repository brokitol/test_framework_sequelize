'use strict';

var app = require('../../server/server');

module.exports = function(Joujou) {

  /**
   * créer un joujou
   * @param {string} name nom du joujou
   * @param {number} toutou_id
   * @param {Function(Error)} callback
   */

  Joujou.create = function(name, toutou_id, callback) {
    console.log("joujou.create("+name+", "+toutou_id+")");
    callback(null);
  };

  Joujou.get = function(id, callback) {
    console.log("joujou.get("+id+")");
    callback(null);
  };

};
