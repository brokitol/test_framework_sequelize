'use strict';

var app = require('../../server/server');

module.exports = function(sequelize, DataTypes) {
  var Toutou = sequelize.define('Toutou', {
    name: DataTypes.STRING,
    ownerId: DataTypes.UUID
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Toutou.belongsTo(models.User, {
          as: 'owner',
          foreignKey: 'ownerId'
        });
        Toutou.hasOne(models.Joujou);
      }
    }
  });
  return Toutou;
};
