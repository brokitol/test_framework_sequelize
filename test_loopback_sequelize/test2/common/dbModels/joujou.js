'use strict';

var app = require('../../server/server');

module.exports = function(sequelize, DataTypes) {
  var Joujou = sequelize.define('Joujou', {
    name: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        Joujou.belongsTo(models.Toutou);
      }
    }
  });
  return Joujou;
};
