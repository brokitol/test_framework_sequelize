'use strict';

module.exports = function(Joujou) {
  Joujou.on('dataSourceAttached', function(obj){
    var find = Joujou.find;
    Joujou.find = function(filter, cb) {

      return find.apply(this, arguments);
    };
  });

  /**
    * un test de l'orm
    * @param {string} name_joujou
    * @param {Function(Error)} callback
    */

  Joujou.test = function(name_joujou, callback) {
    console.log("name_joujou : " + name_joujou);

    /*var filtre = {
      include: {
        relation: 'toutou',
        scope: {
          //where: {id: name_joujou}
          relation: 'prout',
          /*scope: {
            //where: {id: name_joujou}
          }*//*
        }
      }
    }
    console.log("filtre : " + filtre);
    console.log(filtre);
    this.app.models.Joujou.find(
      filtre,
      function(err, joujous) {
        joujous.forEach(function(joujou) {
          var j = joujou.toJSON();
          console.log("debut print for each");
          console.log(j);
          if (j.toutou)
            console.log(j.toutou.name, j.toutou.owner);
          console.log("fin print for each");
        });
        console.log("debut print perso");
        console.log(joujous);
        console.log("fin print perso");
    });*/

    var filtre = {
      include: {
        relation: 'toutous',
        scope: {
          include: {
            relation: 'joujous',
            scope: {
              where: {name: name_joujou}
            }
          }
        }
      }
    }
    console.log("filtre : ");
    console.log(filtre);
    this.app.models.Prout.find(
      filtre,
      function(err, prouts) {
        prouts.forEach(function(prout) {
          var p = prout.toJSON();
          console.log("debut print for each");
          console.log(p);
          if (p.toutous) {
            p.toutous.forEach(function(toutou) {
              console.log(toutou);
            })
          }
          console.log("fin print for each");
        });
        console.log("debut print perso");
        console.log(prouts);
        console.log("fin print perso");
    });


    callback(null);
  };

};
