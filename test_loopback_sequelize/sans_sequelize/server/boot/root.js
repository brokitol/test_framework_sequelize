'use strict';

module.exports = function(server) {
  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  router.get('/', server.loopback.status());
  server.use(router);

  /*server.dataSources.ormloopback.automigrate('toutou', function(err) {
      if (err) throw err;
  });
  server.dataSources.ormloopback.automigrate('joujou', function(err) {
      if (err) throw err;
  });
  server.dataSources.ormloopback.automigrate('prout', function(err) {
      if (err) throw err;
  });*/
  server.dataSources.ormloopback.automigrate('test1');
  server.dataSources.ormloopback.automigrate('test2');
  server.dataSources.ormloopback.automigrate('test3');
  server.dataSources.ormloopback.automigrate('bidon');
  server.dataSources.ormloopback.automigrate('test1test2');

};
