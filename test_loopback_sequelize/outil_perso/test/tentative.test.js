


describe('/Student', function() {
  var server = require("../server/server")
  var request = require("supertest")(server)
  var dataSource = server.dataSource("db", {adapter:"memory"})
  var Student;

  before(function() {
    Student = dataSource.createModel("Student", apps.models.Student)
  })

  beforeEach(function() {
    Student.updateOrCreate({id: 1, points: 5000})
  })

  it("post blabla", function(done) {
    request.post("/api/Students").send({points: 5000}).expect(200, done)
  })

})
