
process.env.NODE_ENV = 'test'
process.env.ADDRESS_SERVER = 'localhost:3000'
var test = require('unit.js');

var server;

before(function(done) {
	server = require('../server/server')
	//server.dataSources.ormloopback.execute('chat', done);
	//server.dataSources.ormloopback.automigrate('chat', done);
	server.models.Chat
	done();
});

beforeEach(function(done) {
	server.models.Chat.create({name: "bernard", age: 10, sex: "male"})
	server.models.Chat.create({name: "bianca", age: 7, sex: "femelle"}).then(done())
});

after(function() {
});


describe('Test', function() {
  describe('test simple', function() {
    it("chat", function(done) {
      server.models.Chat.find().then(function(res) {
        console.log(res);
        done();
      })
			.catch(function (err) {
				console.log(err);
        done();
			})
    })
    it('ajout simple', function(done) {
      var data = {
        title: "le pouvoir du passé simple",
        employees: 5,
        contract_type: "CDI",
        start_date: "01/01/2016",
        end_date: "01/02/2016",
        description: "fait par default",
        base_job_id: 4,
        base_company_location_id: 4
      }
      test.httpAgent(process.env.ADDRESS_SERVER)
      		.post("/api/c/1/experience/add")
          .send(data)
      		.expect('Content-Type', /json/)
      		.expect(200)
      		.end(function(err, res){
            if (err) {
      				test.fail(err.message);
      			}
            //console.log(res);
            //console.log(res.body);
      			test.number(res.body.data).is(3);
      			done();
      		});
    });

  });
});
