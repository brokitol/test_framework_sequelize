var db = require("./config/initializers/index");
const sequelize_fixtures = require('sequelize-fixtures');

sequelize_fixtures.loadFile("test_data.json", db)
	.then(function () {
		db.Joujou.findAll({
			include: [{model:db.Toutou}]
		})
		.then(res => console.log(JSON.stringify(res, null, 2)));

		db.User.findAll({
			include: {
				association: db.User.associations.pet,
				include: {
					association: db.Toutou.associations.Joujou
					//where: {name: "pouet"}
				}
			}
		})
		.then(res => console.log(JSON.stringify(res, null, 2)));
})
