var Sequelize = require("sequelize");
const sequelize_fixtures = require('sequelize-fixtures');

var sequelize = new Sequelize('test_sequ', 'sequ2', 'pass', {
	host: 'localhost',
	port: 5433,
	dialect: 'postgres',

	pool: {
		max: 5,
		min: 0,
		idle: 10000
	},

});

var User = sequelize.define('user', {
	uuid: {
      type: Sequelize.UUID,
      primaryKey: true,
			defaultValue: Sequelize.UUIDV4
  },
	name: {
		type: Sequelize.STRING
	},
	surname: {
		type: Sequelize.STRING
	}
}, {
	freezeTableName: true
});

var Toutou = sequelize.define('toutou', {
	name: {
		type: Sequelize.STRING
	}
});

var Joujou = sequelize.define('joujou', {
	name: {
		type: Sequelize.STRING
	}
});

Toutou.belongsTo(User, {as: 'owner', foreignKey: 'ownerId'});
Toutou.hasOne(Joujou);

User.hasOne(Toutou, {as: 'pet', foreignKey: 'ownerId'});


Joujou.belongsTo(Toutou);

// models a remplir
sequelize_fixtures.loadFile(test_data.json, models)
	.then(function () {
		Joujou.findAll({
			include: [{model:Toutou}]
		})
		.then(res => console.log(JSON.stringify(res, null, 2)));

		User.findAll({
			include: {
				association: User.associations.pet,
				include: {
					association: Toutou.associations.joujou
					//where: {name: "pouet"}
				}
			}
		})
})



/*
sequelize.sync({force: true}).then(function() {
	return User.create({name: 'John', surname: 'Hancock'}).then(function(u1) {
	return User.create({name: 'jack', surname: 'le fou'}).then(function(u2) {
	return Toutou.create({name: 'digo'}).then(function(t1) {
	return Toutou.create({name: 'flufi'}).then(function(t2) {
	return Joujou.create({name: 'pouet'}).then(function(j1) {
	return Joujou.create({name: 'os'}).then(function(j2) {

		u1.setPet(t1).then(function() {
		t2.setOwner(u2).then(function() {
		t2.setJoujou(j2).then(function() {
		j1.setToutou(t1).then(function(){
			Joujou.findAll({
				include: [{model:Toutou}]
			})
			.then(res => console.log(JSON.stringify(res, null, 2)));

			User.findAll({
				include: {
					association: User.associations.pet,
					include: {
						association: Toutou.associations.joujou
						//where: {name: "pouet"}
					}
				}
			})
			.then(res => console.log(JSON.stringify(res, null, 2)));
		});});});});

	});});});});});});
});//*/

//sequelize.sync({force: true}).then(function() {	return User.create({name: 'John', surname: 'Hancock'}).then(function(u1) {	return User.create({name: 'jack', surname: 'le fou'}).then(function(u2) {	return Toutou.create({name: 'digo'}).then(function(t1) {	return Toutou.create({name: 'flufi'}).then(function(t2) {	return Joujou.create({name: 'pouet'}).then(function(j1) {	return Joujou.create({name: 'os'}).then(function(j2) {		u1.setPet(t1);	});});});});});});});
