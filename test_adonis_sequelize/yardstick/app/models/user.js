'use strict';
module.exports = function(sequelize, DataTypes) {
  var User = sequelize.define('User', {
    uuid: {
        type: DataTypes.UUID,
        primaryKey: true,
  			defaultValue: DataTypes.UUIDV4
    },
    name: DataTypes.STRING,
    surname: DataTypes.STRING
  }, {
    classMethods: {
      associate: function(models) {
        // associations can be defined here
        User.hasOne(models.Toutou, {
          as: 'pet',
          foreignKey: 'ownerId'
        });
      },
      * auto_create() {
        var u = yield global.db.User.create({name:"naruto",surname:"le clone de trop"});

        return u;
      }
    }
  });
  return User;
};
