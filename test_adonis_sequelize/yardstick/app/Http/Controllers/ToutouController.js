'use strict'

class ToutouController {

  * create(request, response) {
    const body = request.all();

    var res = yield global.db.Toutou.create({
      name:     body.name,
      ownerId:  body.owner_id
    });

    response.json(res);
  }

  * get(request, response) {
    var id = request.param("id");

    var res = yield global.db.Toutou.findById(id);

    response.json(res);
  }

  * edit(request, response) {
    const body = request.all();
    var id = request.param("id");

    var toutou = yield global.db.Toutou.findById(id);

    toutou.name = body.name || toutou.name;
    toutou.ownerId = body.owner_id || toutou.ownerId;

    var res = yield toutou.save();

    response.json(res);
  }

  * delete(request, response) {
    var id = request.param("id");

    var res = yield global.db.Toutou.destroy({where: {id:id}});

    response.json(res);
  }
}

module.exports = ToutouController
