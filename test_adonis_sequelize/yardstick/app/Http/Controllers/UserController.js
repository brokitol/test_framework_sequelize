'use strict'

class UserController {

	* create(request, response) {
		const body = request.all();

		user.name = body.name;
		user.surname = body.surname;

		var res = yield global.db.User.create({
			name: body.name,
			surname:	body.surname
		});

		response.json(res);
	}

	* get(request, response) {
		var id = request.param("id");
		var res = yield global.db.User.find({
      where: {uuid:id},
      include: [{
        model: global.db.Toutou,
        as: "pet",
        include: [global.db.Joujou]
      }]
    });
		response.json(res);
	}

	* edit(request, response) {
		const body = request.all();
		var id = request.param("id");
		var user = yield global.db.User.findById(id);

		user.name = body.name || user.name;
		user.surname = body.surname || user.surname;

		var res = yield user.save();

		response.json(res);
	}

	* delete(request, response) {
		var id = request.param("id");
		var res = yield global.db.User.destroy({where: {uuid:id}});
		response.json(res);
	}

}

module.exports = UserController
