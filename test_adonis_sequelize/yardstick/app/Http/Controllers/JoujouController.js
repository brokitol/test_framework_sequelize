'use strict'

class JoujouController {

  * create(request, response) {
    const body = request.all();
    
    var res = yield global.db.Joujou.create({
      name:     body.name,
      ToutouId: body.toutou_id
    });

    this.success(res);
  }

  * show(request, response) {
    var id = request.param("id");

    var res = yield global.db.Joujou.findById(id);

    response.json(res);
  }

  * edit(request, response) {
    const body = request.all();
    var id = request.param("id");

    var joujou = yield global.db.Joujou.findById(id);

    joujou.name = body.name || joujou.name;
    joujou.ToutouId = body.toutou_id || joujou.ToutouId;

    var res = yield res.save();

    response.json(res);
  }

  * destroy(request, response) {
    var id = request.param("id");

    var res = yield global.db.Joujou.destroy({where: {id:id}});

    response.json(res);
  }

}

module.exports = JoujouController
