'use strict'

const Env = use('Env')


class PostController {
  * index (request, response) {
    console.log("PostController.index est bien appelé")

    global.db.User.findAll().then(function(a) {for (var k in a) {console.log(a[k].dataValues)}}).catch(console.log);
    yield response.sendView('home')
  }

  * create (request, response) {
    console.log("PostController.create est bien appelé")
    var res = yield global.db.User.auto_create();
    response.json(res);
  }
}

module.exports = PostController
