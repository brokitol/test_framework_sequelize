'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.get('/', 'PostController.index')

Route.post('/user/create', 'UserController.create')
Route.put('/user/:id', 'UserController.edit')
Route.get('/user/:id', 'UserController.get')
Route.delete('/user/:id', 'UserController.delete')

Route.post('/toutou/create', 'ToutouController.create')
Route.put('/toutou/:id', 'ToutouController.edit')
Route.get('/toutou/:id', 'ToutouController.get')
Route.delete('/toutou/:id', 'ToutouController.delete')

Route.post('/joujou/create', 'JoujouController.create')
Route.put('/joujou/:id', 'JoujouController.edit')
Route.get('/joujou/:id', 'JoujouController.get')
Route.delete('/joujou/:id', 'JoujouController.delete')

Route.on('/about').render('about')
Route.on('/contact').render('contact')
