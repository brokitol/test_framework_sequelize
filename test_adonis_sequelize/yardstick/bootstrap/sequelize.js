'use strict';

var fs        = require('fs');
var path      = require('path');
var Sequelize = require('sequelize');
var basename  = path.basename(module.filename);
var env       = process.env.NODE_ENV || 'development';
var config    = require(__dirname + '/../config/configDB.json')[env];  // fichier de config de sequelize
var dir_models = __dirname + "/../app/models/";  // path vers les models
var db        = {};

if (config.use_env_variable) {
  var sequelize = new Sequelize(process.env[config.use_env_variable]);
} else {
  var sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(dir_models) // lire le dossier contenant les models
  .filter(function(file) { // ne prendre en compte que les fichier .js
    return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) { // les importer effectivement
    var model = sequelize['import'](path.join(dir_models, file));
    db[model.name] = model;
  });

// mettre en place les associations (doit être fait après avoir charger toute les classes)
Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

global.db = db;
