var locomotive = require('locomotive')
  , Controller = locomotive.Controller;

var pagesController = new Controller();

pagesController.main = function() {
  console.log(this.__app.db.User);
  this.__app.db.User.findAll().then(console.log).catch(console.log);
  this.title = 'Locomotive';
  this.render();
}

module.exports = pagesController;
